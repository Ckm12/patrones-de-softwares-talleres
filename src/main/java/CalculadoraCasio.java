import java.util.ArrayList;
import java.util.List;

public class CalculadoraCasio implements Calcuadora {
    String marca;
    String serial;
    List<String> operaciones = new ArrayList<>();


    @Override
    public Integer sumar(Integer numeroUno, Integer numeroDos) {
        int resultado = numeroUno + numeroDos;
        this.operaciones.add("Numero uno: " + numeroUno + " + Numero dos: " + numeroDos + " Resultado: " + resultado);
        return resultado;
    }

    @Override
    public Integer restar(Integer numeroUno, Integer numeroDos) {
        int resultado = numeroUno - numeroDos;
        this.operaciones.add("Numero uno: " + numeroUno + " - Numero dos: " + numeroDos + " Resultado: " + resultado);
        return resultado;
    }

    @Override
    public Double multipli(Integer numeroUno, Integer numeroDos) {
        int resultado = numeroUno / numeroDos;
        this.operaciones.add("Numero uno: " + numeroUno + " / Numero dos: " + numeroDos + " Resultado: " + resultado);
        return (double) resultado;

    }

    @Override
    public Double div(Integer numeroUno, Integer numeroDos) {
        int resultado = numeroUno / numeroDos;
        this.operaciones.add("Numero uno: " + numeroUno + " / Numero dos: " + numeroDos + " Resultado: " + resultado);
        return (double) resultado;
    }

    @Override
    public Integer potencia(Integer numeroUno, Integer numeroDos){
        int resultado= numeroUno^numeroDos;
        this.operaciones.add("Numero uno"+ numeroUno + " / Numero dos: " + numeroDos + " Resultado: " + resultado);
        return  resultado;
    }



}
    /**
    @Override
    public Integer sumar(Integer numeroUno, Integer numeroDos) {
        return null;
    }

    @Override
    public Integer restar(Integer numeroUno, Integer numeroDos) {
        return null;
    }

    @Override
    public Double multipli(Integer numeroUno, Integer numeroDos) {
        return null;
    }

    @Override
    public Double div(Integer numeroUno, Integer numeroDos) {
        return null;
    }
}
   /**
 public Integer sumar(Integer numeroUno, Integer numeroDos){
        return numeroUno + numeroDos;
    };
    public Integer restar (Integer numeroUno,Integer numeroDos){
        return numeroUno - numeroDos;
    };

    public Double multipli (Integer numeroUno,Integer numeroDos){
        return ((numeroUno*1.0) * (numeroDos*1.0));
    };

    public Double div ( Integer numeroUno,Integer numeroDos){
        return numeroUno*1.0 / numeroDos*1.0;
    };
}
*/