public interface Calcuadora {

    Integer sumar(Integer numeroUno, Integer numeroDos) ;

    Integer restar(Integer numeroUno, Integer numeroDos);

    Double multipli(Integer numeroUno, Integer numeroDos);

    Double div(Integer numeroUno, Integer numeroDos);

    Integer potencia(Integer numeroUno, Integer numeroDos);
}